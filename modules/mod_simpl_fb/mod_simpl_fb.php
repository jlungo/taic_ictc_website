<?php

/**
 * @copyright   Copyright (C) 2012 Brian Coale. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
JLoader::register('modSimplFBHelper', dirname(__FILE__).'/helper.php');
$data_result = modSimplFBHelper::getFBPlugin($params);

// Only show module if there is something to show
if (isset($data_result['fb_url'])) {
    $moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
    require JModuleHelper::getLayoutPath('mod_simpl_fb', $params->get('layout', 'default'));
	echo $data_result['mod_data'];
}

?>