/***********************************************************************************************/
/* Main IceTheme template Scripts */
/***********************************************************************************************/


/* default joomla script */
(function($)
{
	$(document).ready(function()
	{
		$('*[rel=tooltip]').tooltip()

		// Turn radios into btn-group
		$('.radio.btn-group label').addClass('btn');
		$(".btn-group label:not(.active)").click(function()
		{
			var label = $(this);
			var input = $('#' + label.attr('for'));

			if (!input.prop('checked')) {
				label.closest('.btn-group').find("label").removeClass('active btn-success btn-danger btn-primary');
				if (input.val() == '') {
					label.addClass('active btn-primary');
				} else if (input.val() == 0) {
					label.addClass('active btn-danger');
				} else {
					label.addClass('active btn-success');
				}
				input.prop('checked', true);
			}
		});
		$(".btn-group input[checked=checked]").each(function()
		{
			if ($(this).val() == '') {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-primary');
			} else if ($(this).val() == 0) {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-danger');
			} else {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-success');
			}
		});
	})
})(jQuery);



/* jQuery scripts for IceTheme template */
jQuery(document).ready(function() {  

	/*  Go to Top Link 
	-------------------*/
	 jQuery(window).scroll(function(){
		if ( jQuery(this).scrollTop() > 1000) {
			 jQuery("#gotop").addClass("gotop_active");
		} else {
			 jQuery("#gotop").removeClass("gotop_active");
		}
	}); 
	jQuery(".scrollup").click(function(){
		jQuery("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});


	/* initialize bootstrap tooltips */
	jQuery("[rel='tooltip']").tooltip();
	
	/* initialize bootstrap popopver */
	jQuery("[rel='popover']").popover();
	
	
	
	/* Languages Module 
	-------------------*/
	/* language module hover efffect for flags */
	jQuery(".mod-languages").hover(function () {
		jQuery(".mod-languages li").css({opacity : .25});
	  }, 
	  function () {
		jQuery(".mod-languages li").css({ opacity : 1});
	  }
	);	
	/* language module remove title attribute from img*/
	jQuery("div.mod-languages img").data("title", jQuery(this).attr("title")).removeAttr("title");
	
	
	/* Social Icons 
	---------------*/
	/* social icons effect on hover */
	jQuery(".social_icons ul").hover(function () {
		jQuery(".social_icons ul li a").css({opacity : .25});
	  }, 
	  function () {
		jQuery(".social_icons ul li a").css({ opacity : 1});
	  }
	);
	
	
	/* Main menu 
	---------------*/
	/* fade slideshow with white bg on menu hover 
	jQuery("#mainmenu").hover(function(){     
		jQuery("#slideshow").addClass("icemegamenu-hover"); 
	},     
	function(){    
	   jQuery("#slideshow").removeClass("icemegamenu-hover"); 
	}); */
	
	/* detect if broser is ipad */
	var iPad_detect = navigator.userAgent.match(/iPad/i) != null;
	if (iPad_detect) {
		jQuery("ul#icemegamenu").addClass("ipad_fix");  
	}
	
	
	/* Search 
	---------*/
	/* adjust the search module */
    jQuery("#search form .inputbox").attr('maxlength','35');
	jQuery("#search form .btn").addClass("icebtn"); 
	
	
	/* Joomla Rating 
	-----------------*/
	/* fix some classes on the content voting */
	jQuery("span.content_vote .btn").removeClass("btn-mini");
	
	
	/* BreadCrumbs  
	--------------*/
	/* fix breadcrumbs */
	jQuery("ul.breadcrumb .divider.icon-location").removeClass("divider icon-location");
	
	
	/* Joomla Articles  
	------------------*/
	/* remove links from article titles */
	jQuery(".item-page .page-header h2 a").contents().unwrap();
	
	
	/* Joomla Newsflas module 
	-------------------------*/ 
	/* only in carousel mode remove introtext but leave the img */
	jQuery('.newsflash-carousel-item p:first-child').contents().filter(function(){ 
		return this.nodeType === 3; 
	}).remove();
	
	
	
	/* login modal box
	------------------*/
	jQuery('.loginModal').click(function(){
		jQuery('#loginModal').modal('show');
	});
	


	/* Slideshow module (CK slideshow) 
	----------------------------------*/
	jQuery(".camera_caption_title, .camera_caption_desc").addClass("container");
	

	
});  

	
// detect if screen is with touch or not (pure JS)
if (("ontouchstart" in document.documentElement)) {
	document.documentElement.className += "with-touch";
}else {
	document.documentElement.className += "no-touch";
}



					
