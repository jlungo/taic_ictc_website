<?php
//  @copyright	Copyright (C) 2013 IceTheme. All Rights Reserved
//  @license	Copyrighted Commercial Software 
//  @author     IceTheme (icetheme.com)

defined('_JEXEC') or die;

// Include main PHP template file
include_once(JPATH_ROOT . "/templates/" . $this->template . '/php/template.php'); 

if ((JRequest::getCmd("tmpl", "index") != "offline")) { ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>

<?php if ($it_responsive == 1) { ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php } ?>

<jdoc:include type="head" />

	<?php
    // Include CSS and JS variables 
    include_once(IT_THEME_DIR.'/php/stylesheet.php');
    ?>
   
</head>

<body class="<?php echo htmlspecialchars($pageclass)?>"> 

	<?php if ($it_usermenu != 0) { ?>
    <div id="usermenu">
        <div class="container"> 
        	<jdoc:include type="modules" name="usermenu" />
        </div> 
    </div>
    <?php } ?>
    
    <div <?php if ($it_slideshow == 0) { ?>id="header_no_slideshow"<?php } ?>>
        
        <div id="header_wrap" class="container">
        
            <header id="header" class="clearfix">  

                <?php if ($it_logo != "") { ?>
                <div id="logo">	
                   <a href="<?php echo $this->baseurl ?>"><?php echo $it_logo_img; ?></a>        	
                </div>
                <?php } ?> 
                    
                <div id="header_right">	
                   
                    <?php if ($it_language != 0) { ?>
                    <div id="language">
                        <jdoc:include type="modules" name="language" /> 
                    </div>
                    <?php } ?>
                    
                    <?php if ($it_search != 0) { ?>
                    <div id="search">
                        <jdoc:include type="modules" name="search" /> 
                    </div>
                    <?php } ?>
                    
                    
                    <?php if ($it_weather != "") { ?>
                       
                    <div id="weather"></div>

                    <script type="text/javascript">
                                
                        jQuery(document).ready(function() {
                          jQuery.simpleWeather({
                            location: ' <?php echo $it_weather; ?>',
                            woeid: '',
                            unit: '<?php echo $it_weather_unit; ?>',
                            success: function(weather) {
                              html = '<i class="w-icon-'+weather.code+'"></i>'+weather.city+', '+weather.region+'';
							  html += ' <span class="weather-temp">'+weather.temp+'&deg;'+weather.units.temp+'</span>';
                              html += ' <span class="currently">'+weather.currently+'</span>';
                          
                              jQuery("#weather").html(html);
                            },
                            error: function(error) {
                              jQuery("#weather").html('<p>'+error+'</p>');
                            }
                          });
                        });
    
                    </script>
                        
                    <?php } ?>
                    
                   
                            
                </div>
                
                 <div id="mainmenu" class="navbar">
                        
                    <div class="navbar-inner">
                    
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        
                        <div class="nav-collapse collapse">
                            <jdoc:include type="modules" name="mainmenu" />
                        </div>     
                        
                    </div>
                
                </div>
            
            </header> 
            
        </div>  
         
	</div>
    
    <?php if ($it_slideshow != 0) { ?>
    <div id="slideshow" >
        <jdoc:include type="modules" name="slideshow" />
    </div>
    <?php } ?>  
             

	<section id="content">  
    
    	<?php if (($it_message || $it_promo) != 0) { ?> 
    	<div class="container">  

            <?php if ($it_message != 0 ) { ?> 
            <div id="message">
            	<jdoc:include type="modules" name="message" style="xhtml" />
            </div>
            <?php } ?>  
    	
			<?php if ($it_promo != 0 ) { ?> 
            <div id="promo">
                <div class="row-fluid">
                    <jdoc:include type="modules" name="promo" style="promo" />
                </div>
            </div>
            <?php } ?>  
      
		</div>
        <?php } ?>
      
      
		<?php if (($it_social_twitter_widget != "") || ($it_social_icons == 1)) { ?> 
        <div id="social_area" class="<?php if (($it_message || $it_promo) == 0) { ?>no-promo<?php } ?>">
        
            <div class="container"> 
                
                <div class="row-fluid">   	
                    
                    <?php if ($it_social_twitter_widget != "") { ?>
                    <div class="span12 <?php if ($it_social_icons == 1) { ?>span8<?php } ?> social_twitter">
                        
                        <script type="text/javascript">
                            twitterFetcher.fetch('<?php echo $it_social_twitter_widget; ?>', 'social_twitter', 1, true);
                        </script>
                        <div id="social_twitter"></div>
        
                    </div>
                    <?php } ?> 
                      
                    <?php if ($it_social_icons == 1) { ?> 
                    <div class="span12 <?php if ($it_social_twitter != "") { ?>span4<?php } ?> social_icons">
                        <ul>
                            
                            <?php if ($it_social_facebook != "" ) { ?> 
                            <li class="social_facebook"><a href="http://www.facebook.com/<?php echo $it_social_facebook; ?>" target="_blank" rel="tooltip" data-placement="bottom" data-original-title="<?php echo JText::_('TPL_SOCIAL_ICONS_FACEBOOK'); ?>"><span><?php echo JText::_('TPL_SOCIAL_ICONS_FACEBOOK'); ?></span></a>
                            </li>
                            <?php } ?> 
                            <?php if ($it_social_twitter  != "" ) { ?> 
                            <li class="social_twitter"><a title="" href="http://www.twitter.com/<?php echo $it_social_twitter; ?>" target="_blank" rel="tooltip" data-placement="bottom" data-original-title="<?php echo JText::_('TPL_SOCIAL_ICONS_TWITTER'); ?>"> <span>"<?php echo JText::_('TPL_SOCIAL_ICONS_TWITTER'); ?></span></a>
                            </li>
                            <?php } ?> 
                            <?php if ($it_social_youtube  != "" ) { ?> 
                            <li class="social_youtube"><a title="" href="http://www.youtube.com/<?php echo $it_social_youtube; ?>" target="_blank" rel="tooltip" data-placement="bottom" data-original-title="<?php echo JText::_('TPL_SOCIAL_ICONS_YOUTUBE'); ?>"> <span><?php echo JText::_('TPL_SOCIAL_ICONS_YOUTUBE'); ?></span></a>
                            </li>
                            <?php } ?> 
                            
                            <?php if ($it_social_rss  != "" ) { ?>  
                            <li class="social_rss"><a title="" href="<?php echo $it_social_rss; ?>/" target="_blank" rel="tooltip" data-placement="bottom" data-original-title="<?php echo JText::_('TPL_SOCIAL_ICONS_RSS'); ?>"> <span><?php echo JText::_('TPL_SOCIAL_ICONS_RSS'); ?></span></a>
                             </li>
                            <?php } ?> 
                            
                        </ul>
                    </div>
                    <?php } ?>
                 
                 </div>   
               
            </div>  
             
        </div>    
        <?php } ?>  
        
        <div class="container">
        
            <div class="row-fluid">
            
                <div id="middlecol" class="<?php echo $content_span;?> <?php echo $sidebar_pos_class; ?>">
                    <div class="inside">
                    
                        <jdoc:include type="message" />
                        
                        <?php if (!isset($it_hide_frontpage)) { ?> 
                        <jdoc:include type="component" />
                        <?php } ?>
                        
                    </div>
                </div>
                    
                <?php if ($it_sidebar != 0) { ?> 
                <div id="sidebar" class="<?php echo $sidebar_span;?> <?php echo $sidebar_pos_class; ?>" >
                    <div class="inside">  
                        
                        <jdoc:include type="modules" name="sidebar" style="sidebar" />
                        
                    </div>
                </div>
                <?php } ?> 
                
            </div> 
            
            <?php if ($it_breadcrumbs != 0) { ?>        
            <div id="breadcrumbs">
				<jdoc:include type="modules" name="breadcrumbs" />
            </div>
            <?php } ?>   
            
             
		</div>  
                        
    </section><!-- / Content  --> 

	
    <footer id="footer">
        	
		<div class="container">

			<?php if ($it_footer != 0) { ?>
            <div class="row-fluid">
                <jdoc:include type="modules" name="footer" style="footer" />
            </div>
            <?php } ?>
                
    	 </div>
         
        <div id="copyright">
        	
            <div class="container"> 
            
            	<?php if ($it_icelogo != 0) { ?> 
                <p id="icelogo">
               		<a href="http://www.icetheme.com" target="_blank" title="We would like to inform that this website is designed by IceTheme.com with the latest standards provied by the World Wide Web Consortium (W3C)"></a>
                </p>
                <?php } ?>      
            
                <p class="copytext">
                     &copy; <?php echo date('Y');?> <?php echo $sitename. "."; ?> 
                     <?php echo JText::_('TPL_RESERVED'); ?> 
                </p> 
                
                <jdoc:include type="modules" name="copyrightmenu" />
                
            </div>
        
        </div>        
         
	</footer> 
	
      
    <?php if ($it_loginModal != 0 ) { ?> 
    <!-- Login Modal -->
    <div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo $modal_login_title; ?></h3>
      	</div>
        
        <div class="modal-body">
			<jdoc:include type="modules" name="modal_login" />
        </div>
        
    </div>
 	<?php } ?>  
    
	<?php if ($it_gotop != 0) { ?>
    <div id="gotop" class="">
        <a href="#" class="scrollup"><?php echo JText::_('TPL_SCROLL'); ?></a>
    </div>
    <?php } ?>  

        
	<?php 
		
		if ($it_slide != 0) { 
			// Include slide module position
			include_once(IT_THEME_DIR.'/php/slide.php');
		}
		
		if ($it_styleswitcher != 0) { 
			// Include style switcher JS 
			include_once(IT_THEME_DIR.'/php/style_switcher.php');
		}
		
		if ($it_pagepeel != "") { 
			// Include PagePeel JavaScript
			include_once(IT_THEME_DIR.'/php/pagepeel.php');
   		} 
		
	?>
    
</body>
</html>
<?php } ?>