<?php
//  @copyright	Copyright (C) 2008 - 2014 IceTheme. All Rights Reserved
//  @license	Copyrighted Commercial Software 
//  @author     IceTheme (icetheme.com)

// No direct access.
defined('_JEXEC') or die;

// Define Variables for template paramters
$it_logo 					= $this->params->get('logo');
$it_logo_img				= '<img class="logo" src="'. JURI::root() . $this->params->get('logo') .'" alt="'. $sitename .'" />';
$it_gotop				   = $this->params->get('go2top');
$it_styleswitcher		   = $this->params->get('styleswitcher');
$it_sidebar_pos    		 = $this->params->get('sidebar_position');
$it_sidebar_width		   = $this->params->get('sidebar_span');
$it_cuosom_css			  = $this->params->get('custom_css_code');
$it_template_style		  = $this->params->get('enable_template_style');
$it_icelogo        	 	 = $this->params->get('icelogo');
$it_icelogo_title           = "We would like to inform that this website is designed by IceTheme.com with the latest standards provied by the World Wide Web Consortium (W3C)";
$it_hide_frontpage_param     = $this->params->get('hidefrontpage');

$it_offline_newsletter      = $this->params->get('offline_newsletter');
$it_offline_h2		      = $this->params->get('offline_h2');
$it_offline_count	       = $this->params->get('offline_count');
$it_offline_count_time      = $this->params->get('offline_count_time');
$it_offline_p		       = $this->params->get('offline_p');
$it_offline_social          = $this->params->get('offline_social');
$it_offline_form		    = $this->params->get('offline_form');

$it_pagepeel      			= JURI::root() . $this->params->get('pagepeel');
$it_pagepeel_link      	   = $this->params->get('pagepeel_link');
$it_pagepeel_fixed      	  = $this->params->get('pagepeel_fixed');
$it_pagepeel_size_s         = $this->params->get('pagepeel_size_s');
$it_pagepeel_size_b      	 = $this->params->get('pagepeel_size_b');

$it_responsive        	  = $this->params->get('responsive_template');
$it_advanced_bootstrap      = $this->params->get('advanced_bootstrap');
$it_advanced_icomoon      	= $this->params->get('advanced_icomoon');
$it_advanced_fontawesome    = $this->params->get('advanced_fontawesome');

$it_background_color        = $this->params->get('backround_color');
$it_background_image		= $this->params->get('backround_image');
$it_background_position     = $this->params->get('backround_position');
$it_background_repeat       = $this->params->get('backround_repeat');


// Define Variables for module positions
$it_usermenu				= $this->countModules('usermenu');
$it_language				= $this->countModules('language');
$it_topmenu  	  	     	 = $this->countModules('topmenu');
$it_mainmenu				= $this->countModules('mainmenu');
$it_search		          = $this->countModules('search');
$it_slideshow			   = $this->countModules('slideshow');
$it_breadcrumbs			 = $this->countModules('breadcrumbs');
$it_icecarousel    		 = $this->countModules('icecarousel');
$it_sidebar    			 = $this->countModules('sidebar');
$it_copyrightmenu     	   = $this->countModules('copyrightmenu');
$it_slide   	 			   = $this->countModules('slide');
$it_loginModal  	 		  = $this->countModules('modal_login');

global $it_promo; 			// we use this variable on modules.php functions
$it_promo	 			   = $this->countModules('promo');
global $it_showcase; 		 // we use this variable on modules.php functions
$it_showcase	 			= $this->countModules('showcase');
global $it_footer; 		   // we use this variable on modules.php functions
$it_footer	 			  = $this->countModules('footer');
global $it_banner; 		   // we use this variable on modules.php functions
$it_banner	 			  = $this->countModules('banner');

// Social Media 
$it_social_twitter_widget   = $this->params->get('social_twitter_widget');

$it_social_facebook		 = $this->params->get('social_icons_facebook');
$it_social_twitter		  = $this->params->get('social_icons_twitter');
$it_social_youtube		  = $this->params->get('social_icons_youtube');
// $it_social_google		   = $this->params->get('social_icons_google');
// $it_social_linkedin		 = $this->params->get('social_icons_linkedin');
$it_social_rss		      = $this->params->get('social_icons_rss');

$it_social_icons		    = (($it_social_facebook !="")  || 
							  ($it_social_twitter !="")   ||
							  ($it_social_youtube !="")   ||
							  ($it_social_rss !=""));

// Only to this template
$it_message		    	 = $this->countModules('message');
$it_weather		    	 =  $this->params->get('weather');
$it_weather_unit		    =  $this->params->get('weather_unit');




/* get the login module title */
jimport( 'joomla.application.module.helper' );
$get_login_module = JModuleHelper::getModule( 'login');
$modal_login_title = $get_login_module->title;

?>