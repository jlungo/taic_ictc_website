<?php
//  @copyright	Copyright (C) 2008 - 2014 IceTheme. All Rights Reserved
//  @license	Copyrighted Commercial Software 
//  @author     IceTheme (icetheme.com)

// No direct access.
defined('_JEXEC') or die;

?>

<script type="text/javascript">
jQuery(function(){
  jQuery('body').peelback({
	adImage  : '<?php echo $it_pagepeel; ?>',
	peelImage  : '<?php echo IT_THEME."/assets/images/pagepeel.png"; ?>',
	clickURL : '<?php echo $it_pagepeel_link; ?>',
	gaTrack  : true,
	gaLabel  : '',
	smallSize : <?php echo $it_pagepeel_size_s; ?>,
	bigSize : <?php echo $it_pagepeel_size_b; ?>,
	autoAnimate: true
  });
  
  <?php if ($it_pagepeel_fixed == 1) { ?> 
  jQuery("#peelback").addClass("peelback_fixed");
  <?php } ?>

});
</script>  