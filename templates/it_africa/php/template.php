<?php
//  @copyright	Copyright (C) 2008 - 2014 IceTheme. All Rights Reserved
//  @license	Copyrighted Commercial Software 
//  @author     IceTheme (icetheme.com)

// No direct access.
defined('_JEXEC') or die;

// Detecting Joomla Active Variables;
$document = JFactory::getDocument();
$app = JFactory::getApplication();
$sitename = $app->getCfg('sitename');
$lang = JFactory::getLanguage();
$user = JFactory::getUser();

// Current item id
$itemid = JRequest::getVar('Itemid');
$menu = $app->getMenu();
$menu_active = $app->getMenu()->getActive();
$pageclass = '';
if (is_object($menu_active))
$pageclass = $menu_active->params->get('pageclass_sfx');

// Define Constants 
define('IT_THEME', $this->baseurl .'/templates/'. $this->template);
define('IT_THEME_DIR', JPATH_ROOT .'/templates/'. $this->template);

// Include Variables
include_once(IT_THEME_DIR.'/php/variables.php'); 

// Load Bootstrap Frameworks (loads JQuery framework as well)
JHtml::_('bootstrap.framework');

// Load the main template JavaScript File
$document->addScript(IT_THEME .'/assets/js/template.js');

// Load the weather JS file
if ($it_weather != "") { 
$document->addScript(IT_THEME .'/assets/js/jquery.simpleWeather.min.js');
}

// Load Page Peel JavaScript file
if ($it_pagepeel != "") { 
$document->addScript(IT_THEME . '/assets/js/jquery.peelback.js');
}

// Load Page Peel JavaScript file
if ($it_social_twitter_widget != "") { 
$document->addScript(IT_THEME . '/assets/js/latest-tweet.js');
}

// Template Styles 
if(!empty($_COOKIE['templatestyle'])) 
$templatestyle = $_COOKIE['templatestyle'];
else $templatestyle =  $this->params->get('TemplateStyle');


// Hide Frontpage view on homepage
// has paramter as well
if ($menu->getActive() == $menu->getDefault($lang->getTag())) {
	$it_hide_frontpage_menu = 1;
}
else {
	$it_hide_frontpage_menu = 0;
}

if ($it_hide_frontpage_param == 1 && $it_hide_frontpage_menu == 1 ) {
	$it_hide_frontpage = 1;	
}
	
	
// offline page direct link
if (JRequest::getCmd("tmpl", "index") == "offline") {
	require_once(JPATH_ROOT . "/templates/" . $this->template . "/offline.php");  
} 

// Add sidebar class to sidebar div 
if($it_sidebar_pos == 'left') {
	$sidebar_pos_class = 'sidebar_left';	
}
else {
	$sidebar_pos_class = 'sidebar_right';	
}

// sidebar "normal" or "narrow" paramter
if($it_sidebar_width == 'normal') 
{
	$sidebar_span = "span4";
}
else
{
	$sidebar_span = "span3";
}

// Layout Columns width		
if ($it_sidebar != 0)
{ 	
	$content_span = "span8";
}
else
{
	$content_span = "span12";
}

if ($it_sidebar and $it_sidebar_width == 'narrow')
{
	$content_span = "span9";
}


////////////////////////////////////////////////////
// Specific only to this template
////////////////////////////////////////////////////

